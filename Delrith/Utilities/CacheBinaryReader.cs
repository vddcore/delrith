using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace Delrith.Utilities
{
    public class CacheBinaryReader : BinaryReader
    {
        public CacheBinaryReader(string fileName)
            : base(File.OpenRead(fileName))
        {
        }
        
        public CacheBinaryReader(Stream input) 
            : base(input)
        {
        }

        public CacheBinaryReader(Stream input, Encoding encoding) 
            : base(input, encoding)
        {
        }

        public CacheBinaryReader(Stream input, Encoding encoding, bool leaveOpen) 
            : base(input, encoding, leaveOpen)
        {
        }

        public override short ReadInt16()
            => SwapEndianness(BitConverter.ToInt16);

        public override ushort ReadUInt16()
            => SwapEndianness(BitConverter.ToUInt16);

        public override int ReadInt32()
            => SwapEndianness(BitConverter.ToInt32);

        public override uint ReadUInt32()
            => SwapEndianness(BitConverter.ToUInt32);

        public override long ReadInt64()
            => SwapEndianness(BitConverter.ToInt64);

        public override ulong ReadUInt64()
            => SwapEndianness(BitConverter.ToUInt64);

        public override double ReadDouble()
            => SwapEndianness(BitConverter.ToDouble);

        public override float ReadSingle()
            => SwapEndianness(BitConverter.ToSingle);

        public override Half ReadHalf()
            => SwapEndianness(BitConverter.ToHalf);

        public virtual UInt24 ReadUInt24()
        {
            var le = ReadBytes(3);
            Array.Reverse(le);

            return new UInt24(le);
        }

        // public virtual StoreIndex ReadIndex()
        // {
        //     return new StoreIndex(
        //         ReadUInt24(),
        //         ReadUInt24()
        //     );
        // }

        private T SwapEndianness<T>(Func<byte[], int, T> convFunc) where T: struct
        {
            var be = base.ReadBytes(Marshal.SizeOf<T>());
            Array.Reverse(be);

            return convFunc(be, 0);
        }
    }
}