using Delrith.Utilities;

namespace Delrith.JSV5
{
    public struct CacheDataSector
    {
        public const int Size = 520;
        
        public int ContainerID { get; }
        public ushort PositionInSectorList { get; }
        public UInt24 NextSectorNumber { get; }
        public byte IndexFileID { get; }

        public CacheDataSector(int containerId, ushort positionInSectorList, UInt24 nextSectorNumber, byte indexFileId)
        {
            ContainerID = containerId;
            PositionInSectorList = positionInSectorList;
            NextSectorNumber = nextSectorNumber;
            IndexFileID = indexFileId;
        }
    }
}