using System;
using System.IO;
using Delrith.Utilities;

namespace Delrith.JSV5
{
    public class CacheDataFile : IDisposable
    {
        private FileStream _fs;
        private CacheBinaryReader _cbr;
        
        public string FilePath { get; }

        public CacheDataFile(string filePath)
        {
            FilePath = filePath;

            _fs = File.OpenRead(filePath);
            _cbr = new CacheBinaryReader(_fs);
        }
        
        public void Dispose()
        {
            _fs.Dispose();
            _cbr.Dispose();
        }
    }
}