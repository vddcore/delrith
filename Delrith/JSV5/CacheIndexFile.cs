using System;
using System.IO;
using System.Text;
using Delrith.Utilities;

namespace Delrith.JSV5
{
    public class CacheIndexFile : IDisposable
    {
        private FileStream _fs;
        private CacheBinaryReader _cbr;
        
        public string FilePath { get; }
        public int IndexCount { get; }

        public CacheIndex this[int number]
        {
            get
            {
                _fs.Seek(number * CacheIndex.Size, SeekOrigin.Begin);
            
                return new CacheIndex(
                    _cbr.ReadUInt24(),
                    _cbr.ReadUInt24()
                );
            }
        }

        public CacheIndexFile(string filePath)
        {
            FilePath = filePath;

            _fs = File.OpenRead(filePath);
            _cbr = new CacheBinaryReader(_fs, Encoding.UTF8, true);

            IndexCount = (int)(_fs.Length / CacheIndex.Size);
        }

        public void Dispose()
        {
            _fs.Dispose();
            _cbr.Dispose();
        }

        public override string ToString()
            => $"{FilePath}: {IndexCount} indices, {_fs.Length} bytes";
    }
}