using System;
using System.IO;
using System.Linq;
using System.Text;

namespace Delrith.JSV5
{
    public class Cache : IDisposable
    {
        public string DirectoryPath { get; }

        public CacheIndexFile[] Volumes { get; } = new CacheIndexFile[256];
        public int DataVolumeCount { get; private set; }

        public Cache(string directoryPath)
        {
            DirectoryPath = directoryPath;

            ReadVolumes();
        }

        private void ReadVolumes()
        {
            var files = Directory.GetFiles(DirectoryPath, "*.idx*");

            if (!files.Any(x => x.EndsWith(".idx255")))
                throw new InvalidDataException("Cache is invalid: no master index table.");

            DataVolumeCount = files.Length - 1;
            
            for (var i = 0; i < files.Length; i++)
            {
                var filePath = files[i];
                var index = byte.Parse(Path.GetExtension(filePath).Replace(".idx", ""));

                Volumes[index] = new CacheIndexFile(filePath);
            }

            for (var i = 0; i < DataVolumeCount /* ignore the 255 file */; i++)
            {
                if (Volumes[i] == null)
                {
                    throw new InvalidDataException("Cache is invalid: index files are not sequential.");
                }
            }
        }

        public void Dispose()
        {
            for (var i = 0; i < 255; i++)
            {
                if (Volumes[i] != null)
                {
                    Volumes[i].Dispose();
                    Volumes[i] = null;
                }
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            
            sb.AppendLine($"Cache {DirectoryPath}: {DataVolumeCount} data volumes, 1 master volume, {Volumes.Count(x => x != null)} total loaded.");
            sb.AppendLine("{");
            for (var i = 0; i < DataVolumeCount; i++)
            {
                sb.AppendLine($"  {Volumes[i].ToString()}");
            }
            
            sb.AppendLine($"  {Volumes[255].ToString()}");
            sb.AppendLine("}");
            return sb.ToString();
        }
    }
}