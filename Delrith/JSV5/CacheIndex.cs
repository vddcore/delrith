using Delrith.Utilities;

namespace Delrith.JSV5
{
    public struct CacheIndex
    {
        public const int Size = 6;
        
        public UInt24 DataSize { get; }
        public UInt24 StartingSector { get; }

        public CacheIndex(UInt24 dataSize, UInt24 startingSector)
        {
            DataSize = dataSize;
            StartingSector = startingSector;
        }
    }
}